# Chatty Mango Version Check

This package helps you use the open source analytics software Matomo to log all deployed versions of your WordPress themes and plugins.

## Description

This PHP package consists of a class for use with WordPress as a client and a script that is placed on a server. Together they check if an update for a package is available and they report the installed versions of all packages to a server running [Matomo](https://matomo.org) (formerly Piwik).

The main use is to get an idea of which versions are currently out there and if users of your plugins and themes need to update. The information is not precise since it depends on the frequency how often a plugin or theme contacts the server. You can try to improve the accuracy by using caching on the client side so that the queries don't happen too randomly.

The information that identifies the websites is hashed in order to protect the client sites from revealing excessive data.

By default, the result is cached for 6 hours.

## Preconditions and Installation

See [How To Use Matomo To Log Version Statistics From An Update Checker](https://chattymango.com/how-to-use-matomo-to-log-version-statistics-from-an-update-checker/) for a detailed description.

You need a working server running Matomo. It is not necessary, however, that it runs on the same server as the "server.php" script.

On Matomo do the following:

- create a new website
- for that website, create a goal "update available"
- also there, create a custom dimension (per visit) for each package that you want to track

Copy PiwikTracker.php from Matomo into the same folder where you place server.php and config.php. That location must be accessible from the web.

Adjust the configuration in the config.php script.

Then you require_once the class ChattyMango_VersionCheck in your client plugins and themes.


You can call the class like in the following example.

```
$version_check = new ChattyMango_VersionCheck( 'my_plugin_packages' );

$result = $version_check
                 ->set_url( 'https://www.example.com/server.php' )
                 ->set_name( 'my-plugin' )
                 ->set_current_version( '1.0.1' )
                 ->compare();

if ( is_array( $result ) ) {

  if ( $result['update_available'] ) {

    echo "There's an update available.";

  } else {

    echo "You are using the latest version.";

  }

} else {

  echo "Error code: " . $version_check->last_error;

}
```

You can do that for different packages on the same WordPress site, and Matomo will with each request log all latest available version information for all packages that were checked.


## API

See the documentation in the code, including error codes.


## Author

[Christoph Amthor](https://chattymango.com)

## License

GPL-2.0+ - see the [LICENSE.md](LICENSE.md) file for details.
