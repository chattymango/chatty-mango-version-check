<?php
/**
* Chatty Mango Version Check
*
* @package     Chatty Mango Version Check
* @author      Christoph Amthor
* @copyright   2017 Christoph Amthor (@ Chatty Mango, chattymango.com)
* @license     GPL-2.0+
* @version     2.5
*
*
*/

if ( ! class_exists( 'ChattyMango_VersionCheck' ) ) {

  class ChattyMango_VersionCheck {

    /**
    *  version of this script
    *
    */
    const VERSION = "2.5";


    /**
    *  default identifier used to save the package information in the WordPress options
    *
    */
    const WP_CHATTYMANGO_PACKAGES = 'chatty_mango_packages';


    /**
    * information on all available packages
    *
    * @var array
    */
    var $chatty_mango_packages;


    /**
    * WordPress response
    *
    * @var array
    */
    private $response;


    /**
    * URL from where to retrieve the version information
    * The content should contain the following array, JSON-encoded:
    *
    *  array(
    *    $slug => array(
    *      'version' => $version,
    *      'url' => $url,
    *      'message' => $message
    *    ),
    *     ... optional further elements
    *  )
    *
    * string $slug: identifier of the software to be tested
    * string $version: PHP-compatible version number of the latest available version
    * string $url: optional download URL for the latest version
    * string $message: optional human-readable message for the recipient (warnings, explanations, ...)
    *
    * @var string
    */
    private $url;


    /**
    * arguments for WordPress' wp_remote_get()
    *
    * @var array
    */
    private $args;


    /**
    * package name (usually slug)
    *
    * @var string
    */
    private $name;


    /**
    * current version
    *
    * @var string
    */
    private $current_version ;


    /**
    * identifier of the WordPress option where we save all watched packages
    *
    * @var string
    */
    private $wp_option_name;


    /**
    * last error occured
    *
    * 1 error reading remote file
    * 2 URL missing
    * 3 current_version missing
    * 4 name missing
    * 5 package info not found in remote file
    *
    * @var int
    */
    public $last_error;


    /**
    * time in seconds after which the cache should expire
    *
    * @var string
    */
    private $cache_expire;


    /**
    * Constructor
    *
    *
    * @param string $wp_option_name key used for the WordPress options database table to save all package information
    * @return void
    */
    public function __construct( $wp_option_name = NULL )
    {

      if ( ! empty( $wp_option_name ) ) {

        $this->wp_option_name = $wp_option_name;

      } else {

        $this->wp_option_name = self::WP_CHATTYMANGO_PACKAGES;

      }

      $this->load_defaults();

      $this->register_hooks();

    }


    /**
    * Registers relevant WordPress hooks
    *
    *
    * @param void
    * @return object $this
    */
    private function register_hooks() {

      add_action( 'activated_plugin', array( $this, 'clear_cache' ) );

      add_action( 'deactivated_plugin', array( $this, 'clear_cache' ) );

    }


    /**
    * Deletes the cache so that fresh data will be retrieved
    *
    *
    * @param void
    * @return void
    */
    public function clear_cache()
    {

      delete_transient( 'chatty_mango_version_check' );

    }



    /**
    * Load default values into properties
    *
    *
    * @param void
    * @return object $this
    */
    private function load_defaults()
    {

      global $wp_version;

      $this->args = array(
        'timeout'     => 90,
        'redirection' => 5,
        'httpversion' => '1.0',
        'user-agent'  => 'Mozilla/5.0 (Android 4.4; Mobile; rv:41.0) Gecko/41.0 Firefox/41.0',// Use any that works. Matomo ignores 'WordPress/' . $wp_version . '; '.
        'blocking'    => true,
        'headers'     => array(),
        'cookies'     => array(),
        'body'        => null,
        'compress'    => false,
        'decompress'  => true,
        'sslverify'   => true,
        'stream'      => false,
        'filename'    => null
      );

      $this->cache_expire = 6 * 60 * 60; // default: 6 hours

      $this->last_error = 0;

      $this->chatty_mango_packages = get_option( $this->wp_option_name, array() );

      return $this;

    }


    /**
    * Setter for the cache expiry (WordPress object cache)
    *
    * @param int $cache_expire expiry time in seconds
    * @return object $this
    */
    public function set_cache_expire( $cache_expire )
    {

      $this->$cache_expire = $cache_expire;

      return $this;

    }


    /**
    * Setter for any additional arguments (optional)
    *
    * @param string $key
    * @param mixed $value
    * @return object $this
    */
    public function set_arg( $key, $value )
    {

      $this->args[ $key ] = $value;

      return $this;

    }


    /**
    * Setter for the URL pointing to the server.php script
    *
    * @param string $url
    * @return object $this
    */
    public function set_url( $url = '' )
    {

      $this->url = $url;

      return $this;

    }


    /**
    * Setter for the software (plugin) name to check
    *
    * Use the slug (the folder name) in small letters with dashes, like 'my-plugin'
    *
    * @param string $name Name (slug)
    * @return object $this
    */
    public function set_name( $name = '' )
    {

      $this->name = $name;

      return $this;

    }


    /**
    * Setter for the current version
    *
    * @param string $current_version current version
    * @return object $this
    */
    public function set_current_version( $current_version = '' )
    {

      $this->current_version = $current_version;

      return $this;

    }


    /**
    * Save the information of all packages in the options
    *
    *
    * @param void
    * @return void
    */
    private function save_option()
    {

      $this->chatty_mango_packages[ $this->name ] = $this->current_version;

      update_option( $this->wp_option_name, $this->chatty_mango_packages );

    }


    /**
    * Compares the current version with the latest version
    *
    * The array key 'update_available' tells if update is available. Returns false, if something went wrong.
    *
    * @param void
    * @return array|bool
    */
    public function compare()
    {

      /**
      * check if we have all required information
      */
      if ( empty( $this->current_version ) ) {

        $this->last_error = 3;

        return false;

      }

      if ( empty( $this->name ) ) {

        $this->last_error = 4;

        return false;

      }

      if ( empty( $this->url ) ) {

        $this->last_error = 2;

        return;

      }

      /**
      * Save the package information in the options
      */
      $this->save_option();

      $remote_info = $this->get_remote_info();

      if ( !$this->last_error ) {

        if ( empty( $remote_info[ $this->name ] ) ) {

          $this->last_error = 5;

          return false;

        } else {

          $result = version_compare( $this->current_version, $remote_info[ $this->name ]['version'] );

          if ( $result == -1 ) {

            return array(
              'update_available'  => true,
              'version' => $remote_info[ $this->name ]['version'],
              'message' =>  $remote_info[ $this->name ]['message'],
              'url' => $remote_info[ $this->name ]['url']
            );

          } else {

            return array(
              'update_available'  => false,
              'version' => $remote_info[ $this->name ]['version'],
              'message' =>  $remote_info[ $this->name ]['message'],
              'url' => $remote_info[ $this->name ]['url']
            );

          }

        }

      }

    }


    /**
    * Adds all information that we want to send as GET query parameters
    *
    *
    * @param string $url The URL where to send the query
    * @return string The URL with queries added
    */
    private function add_queries($url)
    {

      $queries = array();

      foreach($this->chatty_mango_packages as $key => $value ) {
        $queries[] = $key . ':' . $value;
      }

      $url = strpos( '?', $this->url ) ? $this->url . '&' : $this->url . '?';

      return $url . 'hash=' . md5( site_url() ) . '&packages=' . implode( ',', $queries );

    }


    /**
    * Retrieve the information about current versions, download URLs and upgrade messages from cache or remote
    *
    * @param void
    * @return array
    */
    private function get_remote_info()
    {

      $url = $this->add_queries( $this->url );

      $key = md5( $url );

      /**
      * Check if the cache returns the required information
      */
      $cache_content = get_transient( 'chatty_mango_version_check' );

      if ( $cache_content !== false && ! empty( $cache_content[ $key ] ) ) {

        return $cache_content[ $key ];

      }

      $request  = wp_remote_get( esc_url_raw( $url ), $this->args );

      $response_code = wp_remote_retrieve_response_code( $request );

      $response_message = wp_remote_retrieve_response_message( $request );

      if ( 200 != $response_code || 'OK' != $response_message ) {

        $this->last_error = 1;

        return;

      } else {

        $response = wp_remote_retrieve_body( $request );

        $result = json_decode( $response, true );

        /**
        * Save the data to the cache
        */
        if ( ! is_array( $cache_content ) ) {

          $cache_content = array();

        }

        $cache_content[ $key ] = $result;

        set_transient( 'chatty_mango_version_check', $cache_content, $this->cache_expire );

        return $result;

      }
    }


  } // class

}
