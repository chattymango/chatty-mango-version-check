<?php
/**
* Edit the following values for your server configuration.
*
*/


/**
* The URL where you have Matomo installed
*
* example:
* $config['matomo_url'] = 'https://matomo.example.com/';
*/
$config['matomo_url'] = '';


/**
* The ID of the website at Matomo where you will track the versions
*/
$config[ 'matomo_website_id' ] = 1;


/**
* Specify a Matomo API token with at least Admin permission
*/
$config['token'] = '';


/**
*  User agent that is accepted by Matomo
*/
$config['user_agent'] = 'Mozilla/5.0 (Android 4.4; Mobile; rv:41.0) Gecko/41.0 Firefox/41.0';


/**
* Slugs of the packages and the corresponding custom dimension in Matomo.
*
* example:
* $config['dimensions'] = array(
* 'my-first-plugin' => 'dimension1',
* 'my-second-plugin' => 'dimension2'
* );
*/
$config['dimensions'] = array(
  ''  => 'dimension1',
);


/**
* The ID of the goal "update available".
*/
$config['goal'] = 1;
