<?php
/**
* Chatty Mango Version Check
*
* @package     Chatty Mango Version Check
* @author      Christoph Amthor
* @copyright   2017 Christoph Amthor (@ Chatty Mango, chattymango.com)
* @license     GPL-2.0+
* @version     2.3
*
*/

/**
* Turn off error reporting
*/
error_reporting(0);


/**
* Load the configuration
*/
require_once './config.php';


/**
* Prepare the PiwikTracker
*/
require_once './PiwikTracker.php';

PiwikTracker::$URL = $config[ 'matomo_url' ];

$piwikTracker = new PiwikTracker( $config[ 'matomo_website_id' ] );

$piwikTracker->setTokenAuth( $config[ 'token' ] );

$piwikTracker->setUserAgent( $config[ 'user_agent' ] );


/**
* Retrieve the package information and sanitize it.
*
* format: package-name:installed version, ...
* e.g. first-package:1.0,second-package:0.9RC
*/
if ( isset( $_GET['packages'] ) ) {

  /**
  * Sanitize the data (max. 100 chars).
  */
  preg_match( '/^([A-Za-z0-9-,:.]{1,100})$/u', $_GET['packages'], $packages_matches );

  if ( ! empty( $packages_matches[0] ) ) {

    $packages = explode( ',', $packages_matches[0] );

  } else {

    $packages = array();

  }

}


/**
* Use the hash to identify returning visitors
*/
if ( isset( $_GET['hash'] ) ) {

  /**
  * Sanitize the data (max. 32 chars).
  */
  preg_match( '/^([A-Za-z0-9]{1,32})$/u', $_GET['hash'], $hash_matches );

  if ( ! empty( $hash_matches[0] ) ) {

    $piwikTracker->setUserId( $hash_matches[0] );

  }

}


/**
* Get the up-to-date information about package versions
*/
$latest_versions_json = @file_get_contents( './version-info.json' );

$latest_versions = json_decode( $latest_versions_json, true );

/**
* Log of the transferred information
*/
if ( ! empty( $packages ) ) {

  foreach ( $packages as $package_key_value ) {

    $package_info = explode( ':', $package_key_value );

    if ( ! empty( $config['dimensions'][ $package_info[0] ] ) ) {

      $dimension = $config['dimensions'][ $package_info[0] ];

    } else {

      $dimension = 'unknown package';

    }

    $piwikTracker->setCustomTrackingParameter( $dimension, $package_info[1] );


    /**
    * Send a tracker request via http
    */
    $piwikTracker->doTrackPageView( $package_info[0] );

    if ( ! empty( $latest_versions ) ) {

      /**
      * If the installed package is lagging behind, trigger the goal.
      */
      if ( isset( $latest_versions[ $package_info[0] ]['version'] )
      && version_compare( $version, $latest_versions[ $package_info[1] ]['version'] ) == -1 ) {

        $piwikTracker->doTrackGoal( $config['goal'] );

      }

    }

  }

}

/**
* Return the version information to the ChattyMango_VersionCheck class
*/
header('Content-Type: application/json');

echo $latest_versions_json;
